package com.netty.utils;

import io.netty.handler.codec.http.multipart.InterfaceHttpData;

import java.text.DecimalFormat;
import java.util.HashMap;

public class Utils {

    public static String getUploadFileName(InterfaceHttpData data) {
        String content = data.toString();
        String temp = content.substring(0, content.indexOf("\n"));
        content = temp.substring(temp.lastIndexOf("=") + 2, temp.lastIndexOf("\""));
        return content;
    }

    public static HashMap getSize(long size) {
        //获取到的size为：1705230
        int GB = 1024 * 1024 * 1024;//定义GB的计算常量
        int MB = 1024 * 1024;//定义MB的计算常量
        int KB = 1024;//定义KB的计算常量
        DecimalFormat df = new DecimalFormat("0.00");//格式化小数
        HashMap hashMap=new HashMap();
        String resultSize = "";
        if (size / GB >= 1) {
            //如果当前Byte的值大于等于1GB
            resultSize = df.format(size / (float) GB);
            hashMap.put("unit","GB");
            hashMap.put("resultSize",resultSize);
        } else if (size / MB >= 1) {
            //如果当前Byte的值大于等于1MB
            resultSize = df.format(size / (float) MB);
            hashMap.put("unit","MB");
            hashMap.put("resultSize",resultSize);
        } else if (size / KB >= 1) {
            //如果当前Byte的值大于等于1KB
            resultSize = df.format(size / (float) KB);
            hashMap.put("unit","KB");
            hashMap.put("resultSize",resultSize);
        } else {
            resultSize = size+"";
            hashMap.put("unit","B");
            hashMap.put("resultSize",resultSize);
        }
        return hashMap;
    }

}
